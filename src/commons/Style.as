package commons
{
    import flash.display.Sprite;
    import flash.filters.BitmapFilter;
    import flash.filters.ColorMatrixFilter;
    import flash.filters.GlowFilter;
    import flash.text.TextFormat;

    /**
     * 所有的样式在此定义
     * @author Dont小鬼
     */

    public class Style
    {
        public static var linkColor:uint = 0x58ff00;
        public static var hiliteColor:uint = 0xffc800;
        public static var redColor:uint = 0xff0000;
        public static var greenColor:uint = 0x00ff00;
        public static var orangeColor:uint = 0xFF6100;
        public static var blueColor:uint = 0x0000ff;
        public static var purpleColor:uint = 0x800080;
        public static var yellowColor:uint = 0xffff00;

        /** 淡灰色  */
        public static const Light_Grey:uint = 0xB8A08B;

        /** 深黄色 */
        public static const Deep_Yellow:uint = 0xFF9F08;

        public static var commonFotmatCenter:TextFormat = new TextFormat(null
            , 12, 0xd6f9ff, null, null, null, null, null, "center");
        public static var commonFotmatLeft:TextFormat = new TextFormat(null, 12
            , 0xd6f9ff); //普通文字描述

        /**
         * 获取灰色滤镜值
         * @return Array
         */
        public static const greyFilters:Array = [new ColorMatrixFilter([
            1, 0, 0, 0, 0,
            1, 0, 0, 0, 0,
            1, 0, 0, 0, 0,
            0, 0, 0, 1, 0
            ])];

        /**
         * 获取浅灰色滤镜值
         */
        public static const slightGreyFilter:Array = [new ColorMatrixFilter([0.3086
            , 0.6094, 0.082, 0, 0, 0.3086, 0.6094, 0.082, 0, 0, 0.3086, 0.6094
            , 0.082, 0, 0, 0, 0, 0, 1, 0])];

        /**
         *获取蓝色滤镜值
         */
        public static const blueFilter:Array = [new GlowFilter(0x0066FF, 1
            , 12, 12, 3)];

        /**
         *获取黄色滤镜值
         */
        public static const yellowFilter:Array = [new GlowFilter(0xffff00, 1
            , 6, 6, 3)];

        /**
         *获取红色滤镜值
         */
        public static const redFilter:Array = [new GlowFilter(0xff0000, 1
            , 6, 6, 3)];

        public static function font(content:String, color:String
            , size:int = 12):String
        {
            return "<font color='" + color + "' size='" + size + "'>" + content + "</font>";
        }

        public static function font2(content:String, color:uint
            , size:int = 12):String
        {
            return font(content, "#" + color.toString(16), size);
        }

        public static function drawRect(parent:Sprite, w:Number, h:Number
            , thickness:uint = 2, color:uint = 0xffff00, offW:Number = 0.5
            , offH:Number = 0.5):void
        {
            parent.graphics.lineStyle(thickness, color);
            parent.graphics.moveTo(-offW, -offH);
            parent.graphics.lineTo(w + offW, -offH);
            parent.graphics.lineTo(w + offW, h + offH);
            parent.graphics.lineTo(-offW, h + offH);
            parent.graphics.lineTo(-offW, -offH);
            parent.graphics.endFill();
        }

        /**
         * 根据类型获取对应的显示颜色字符串，用于htmlText
         * @return String
         */
        public static function toColorStr(type:uint):String
        {
            var colorStr:String = type.toString(16);
            return "#" + colorStr;
        }

        /**
         * 根据16进制色码值(rgb)和alpha值(100%的百分整数值)，获取ColorMatrixFilter
         * 为方便调整alpha，参数为2个，不传入argb
         * eg. rgb = 0xBC210B, alpha = 100
         */
        public static function getColorMatrixFilter(rgb:uint
            , alpha:uint = 100):Array
        {
            var red:Number = (rgb & 0x00FF0000) >>> 16;
            var green:Number = (rgb & 0x0000FF00) >>> 8;
            var blue:Number = rgb & 0x000000FF;

            var a:Number = alpha / 100;

            var r:Number = red / 255;
            var g:Number = green / 255;
            var b:Number = blue / 255;

            var matrix:Array = new Array();
            matrix = matrix.concat([r, 0, 0, 0, 0]); // red
            matrix = matrix.concat([0, g, 0, 0, 0]); // green
            matrix = matrix.concat([0, 0, b, 0, 0]); // blue
            matrix = matrix.concat([0, 0, 0, a, 0]); // alpha

            var filter:BitmapFilter = new ColorMatrixFilter(matrix);
            return (new Array(filter));
        }
    }
}
