package commons
{
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;

	public class MovieClipData
	{


		private static var _instance:Dictionary=new Dictionary();


		private var offsetX:Number=200;
		private var offsetY:Number=240;


		public var bitmapFrameList:Array;
		public var labelDic:Dictionary;
		public var url:String=null;
		public var canGC:Boolean;
		public var register:int; //////被使用计数器
		public var lastUserTime:Number=0;
		private var _mc:MovieClip;

		public function get mc():MovieClip
		{
			return _mc;
		}

		public function set mc(value:MovieClip):void
		{
			_mc=value;
			if (_mc == null)
				return;
			if (bitmapFrameList == null)
			{
				bitmapFrameList=new Array();
				labelDic=new Dictionary();
				for (var i:int=0; i < mc.totalFrames; i++)
				{
					bitmapFrameList.push(null);
				}
			}
		}

		public static function dispose():void
		{
			for each (var i:MovieClipData in _instance)
			{
				if (i.canGC)
					i.dispose();
			}
		}

		public function MovieClipData()
		{
			bitmapFrameList=new Array();
			labelDic=new Dictionary();
			_instance[this]=this;
		}

		public function dispose():void
		{
			for each (var i:BitmapFrame in bitmapFrameList)
			{
				if (null != i)
				{
					i.dispose();
				}
			}

			bitmapFrameList=null;
			labelDic=null;
			url=null;
			mc=null;
			delete _instance[this];
		}

		public function get length():int
		{
			if (bitmapFrameList == null)
				return 0;
			return bitmapFrameList.length;
		}

		public function getbitmapFrame(frame:int):BitmapFrame
		{
			var bitmapFrame:BitmapFrame;
			if (mc)
			{
				var rect:Rectangle;
				mc.gotoAndStop(frame);
				rect=mc.getBounds(mc);
				if (rect.height < 1 || rect.width < 1)
				{
					bitmapFrame=null
				}
				else
				{

					var bitmapdata:BitmapData=new BitmapData(rect.width, rect.height, true, 0);
					bitmapdata.draw(mc, new Matrix(1, 0, 0, 1, -rect.x, -rect.y), null, null, null, false);
					bitmapFrame=new BitmapFrame();
					bitmapFrame.bitmapData=bitmapdata;
					bitmapFrame.label=mc.currentLabel;
					bitmapFrame.rx=rect.x - offsetX;
					bitmapFrame.ry=rect.y - offsetY;
				}
			}

			return bitmapFrame;
		}

	}
}

