package commons
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;

	/**
	 * 针对缩放背景封装的类，主要封装了初始化和调整大小。适用于窗口背景，以及一些需要缩放图片的位置
	 * <br><b>使用实例：</b>
	 * <listing>
	 * var rect :Rectangle = new Rectangle(20, 30, 120, 65);
	 * var simage :FioScaleImage = new FioScaleImage;
	 * //simage.setBitmapData(dis.bitmapData.clone(), rect);			//通过已经加载的图片来设置
	 * simage.reSize(400, 400);
	 * addChild(simage);
	 * </listing>
	 * **/
	public class FioScaleImage extends Sprite
	{
		private var rect:Rectangle=null;
		private var viewRect:Rectangle=null;

		private var _width:Number=0;
		private var _height:Number=0;

		public function FioScaleImage()
		{
			super();
		}

		public function removeFromStage():void
		{
			graphics.clear();
			scale9Grid=null;
			rect=null;
		}

		/** 禁止向里面添加显示对象 **/
		override public function addChild(child:DisplayObject):DisplayObject
		{
			throw new Error('[ERROR] FioScaleImage 不允许添加显示对象');
			return null;
		}

		/** 禁止向里面添加显示对象 **/
		override public function addChildAt(child:DisplayObject, index:int):DisplayObject
		{
			throw new Error('[ERROR] FioScaleImage 不允移除显示对象');
			return null;
		}

		/**
		 * 获取缩放区域的左上角的坐标和右边及下面的间隔距离。width=右边间隔距离，height=下面间隔距离
		 * @return 获取缩放区域的左上角的坐标和右边及下面的间隔距离。width=右边间隔距离，height=下面间隔距离
		 */
		override public function get scale9Grid():Rectangle
		{
			return viewRect;
		}

		/**
		 * 设置图片数据源以及缩放九宫格
		 * @param _bitmapData					图片数据源
		 * @param _scaleRect					缩放九宫格
		 */
		public final function setBitmapData(_bitmapData:BitmapData, _scaleRect:Rectangle=null):void
		{
			rect=_scaleRect;
			if (_bitmapData)
			{
				if (null == rect)
				{
					rect=new Rectangle(1, 1, _bitmapData.width - 2, _bitmapData.height - 2);
				}
				viewRect=rect.clone();
				viewRect.width=_bitmapData.width - rect.right;
				viewRect.height=_bitmapData.height - rect.bottom;

				drawBitmap(rect, _bitmapData);
				scale9Grid=rect;
				if (_width > 0 && _height > 0)
				{
					reSize(_width, _height);
				}
			}
			rect=null;
			_bitmapData=null;
			_scaleRect=null;
		}

		private final function drawBitmap(rect:Rectangle, bitmapData:BitmapData):void
		{
			if (null == bitmapData)
				return;
			var _bitmapData:BitmapData=bitmapData.clone();
			bitmapData=null;
			graphics.clear();
			var w:Number=_bitmapData.width;
			var h:Number=_bitmapData.height;
			var mat:Matrix=new Matrix(1, 0, 0, 1, 0, 0);
			var origin:Rectangle=new Rectangle(0, 0, rect.x, rect.y);
			var xs:Array=[0, rect.x, rect.right];
			var ws:Array=[rect.x, rect.width, w - rect.right];
			var ys:Array=[0, rect.y, rect.bottom];
			var hs:Array=[rect.y, rect.height, h - rect.bottom];
			var i:int=0;
			var mod:int;
			var rest:int;
			for (i; i < 9; i++)
			{
				mod=i % 3;
				rest=i / 3;
				graphics.beginBitmapFill(_bitmapData, mat, false, true);
				graphics.drawRect(xs[mod], ys[rest], ws[mod], hs[rest]);
				graphics.endFill();
			}
			_bitmapData=null;
			mat=null;
			origin=null;
			xs=null;
			ys=null;
			ws=null;
			hs=null;
			rect=null;

		}

		override public function set width(value:Number):void
		{
			if (value != _width)
			{
				_width=value;
				super.width=value;
			}
		}

		override public function set height(value:Number):void
		{
			if (value != _height)
			{
				_height=value;
				super.height=_height;
			}
		}

		/**
		 * 重绘大小。如果未设置数据源级九宫格，则不响应
		 * @param _width				新的宽度
		 * @param _height				新的高度
		 */
		public final function reSize(_width:int, _height:int):void
		{
			width=_width;
			height=_height;
		}
	}
}
