package commons
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;

	import webgame.utils.timer.TimerManager;

	public class BitmapMovieClip extends Sprite
	{

		public static var END:String="end";
		private var _bitmap:Bitmap;
		private var offsetX:Number=200;
		private var offsetY:Number=240;

		protected var _endFunction:Function;
		private var _delay:int=-1; //频率

		public var totalFrames:int;
		protected var _startFrame:int;
		protected var _endFrame:int;
		public var currentFrame:int=1; ////当前帧
		protected var _isLoop:Boolean;
		private var _isSmooth:Boolean=false;

		private var _movieClipData:MovieClipDataContainer;

		public function BitmapMovieClip()
		{
			super();
			_bitmap=new Bitmap(null, "auto", true);
			this.addChild(_bitmap);
			this.mouseChildren=false;
			this.mouseEnabled=false;
		}

		public function set movieClipDataContainer(value:MovieClipDataContainer):void
		{
			if (value == null)
				return;
			if (_movieClipData != null)
			{
				_movieClipData.dispose();
				_movieClipData=null;
			}
			_movieClipData=value;
			totalFrames=_movieClipData.length;
			_startFrame=1;
			_endFrame=totalFrames;
			//	this.gotoAndStop(1);
			this.gotoAndPlay(currentFrame);
		}

		public function get movieClipDataContainer():MovieClipDataContainer
		{
			return _movieClipData;
		}

		public function get bitmap():Bitmap
		{
			return _bitmap;
		}

		public function get bitmapData():BitmapData
		{
			return _bitmap.bitmapData;
		}

		public function set bitmapData(bit:BitmapData):void
		{
			_bitmap.bitmapData=bit;
		}

		public function set delay(value:int):void
		{
			_delay=value;
		}

		public function get delay():int
		{
			if (_delay <= 0)
				return 1000 / 30;
			return _delay;
		}



		protected var prePlayTimer:Number=0;

		protected function step():void
		{
			if (currentFrame >= _endFrame)
			{
				if (_isLoop == false)
				{
					stop();
					if (_endFunction != null)
					{
						_endFunction();
					}

					this.dispatchEvent(new Event(END));
					return;
				}
				currentFrame=_startFrame;
			}
			else
			{
				currentFrame++;
			}
			if (_movieClipData == null)
				return;
			setCurrentFrame(currentFrame);
		}




		protected function setCurrentFrame(frameIndex:int):void
		{
			prePlayTimer=getTimer();
			if (frameIndex > _movieClipData.length)
			{
				frameIndex=_movieClipData.length;
			}

			currentFrame=frameIndex;

			var bitmapFrame:BitmapFrame=_movieClipData.getbitmapFrame(currentFrame) as BitmapFrame;


			if (bitmapFrame == null)
			{
				return;
			}
			_bitmap.bitmapData=null;
			_bitmap.bitmapData=bitmapFrame.bitmapData;
			_bitmap.smoothing=_isSmooth;
			_bitmap.x=bitmapFrame.rx;
			_bitmap.y=bitmapFrame.ry;
		}


		public function gotoAndPlay(frame:Object, isLoop:Boolean=true, startFrame:Object=null, endFrame:Object=null, endfunc:Function=null):void
		{

			prePlayTimer=0;
			if (_movieClipData == null)
				return;
			_isLoop=isLoop;
			if (frame is Number)
			{
				currentFrame=int(frame);
			}

			if (startFrame == null)
			{
				_startFrame=1;
			}
			else
			{
				if (startFrame is Number)
				{
					_startFrame=int(startFrame);
				}

			}

			if (endFrame == null)
			{
				_endFrame=totalFrames;
			}
			else
			{

				if (endFrame is Number)
				{
					_endFrame=int(endFrame);
				}

			}

			_endFunction=endfunc;
			setCurrentFrame(currentFrame);
			play();

		}

		public function gotoAndStop(frame:Object):void
		{
			if (_movieClipData == null)
				return;
			if (frame is Number)
			{
				currentFrame=int(frame);
			}

			setCurrentFrame(currentFrame);
			stop();
		}



		public function prevFrame():void
		{
			gotoAndStop(Math.max(currentFrame - 1, 1));
		}

		public function nextFrame():void
		{
			gotoAndStop(Math.min(currentFrame + 1, _movieClipData.length));
		}

		public function stop():void
		{
			prePlayTimer=0;
			TimerManager.getInstance().remove(step);

		}

		public function play():void
		{
			if (delay)
			{
				TimerManager.getInstance().remove(step)
				TimerManager.getInstance().add(delay, step);
				prePlayTimer=0;
			}
		}

		public function getMaxY():Number ////获取y的顶点值
		{
			var rect:Rectangle=_bitmap.getBounds(this);
			return Math.abs(rect.y);
		}

		/**
		 * 设置平滑
		 */
		public function set isSmooth(value:Boolean):void
		{
			_isSmooth=value;
		}

		public function dispose():void
		{
			_endFunction=null;
			_bitmap.bitmapData=null;
			TimerManager.getInstance().remove(step);
			if (_movieClipData != null)
			{
				_movieClipData.dispose();
				_movieClipData=null;
			}

		}
	}
}

