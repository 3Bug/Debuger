package commons
{
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;

	import commons.cache.SourceCache;

	import events.loader.CacheEvent;

	/**
	 * 简易特效播放工具
	 * @author Dont小鬼
	 */
	public class Effect extends EventDispatcher
	{
		/** 单循环播放完成，清理对象后派发事件类型 */
		public static const DISPOSE_EFFECT:String="DisposeEffect";
		/** 特效加入舞台后的事件 */
		public static const ADDED_EFFECT:String="AddedEffect";

		private var bitmapMovieClip:BitmapMovieClip;
		private var _url:String;
		private var _delay:int;
		private var _isLoop:Boolean;
		private var _count:int; //播放次数
		private var _parent:DisplayObjectContainer;
		private var _isAtToZero:Boolean=false;
		private var _visible:Boolean=true;

		public function Effect()
		{
		}

		/**
		 * new完对象后，直接用这个方法播放特效。同时parent可指定加入父容器。
		 * isLoop: 循环播放，否则默认播一次
		 */
		public function showEffect(url:String, parent:DisplayObjectContainer, _x:int=0, _y:int=0, delay:int=100, isLoop:Boolean=false, isScaleX:Boolean=false, count:int=1, isAtToZero:Boolean=false):void
		{

			_parent=parent;
			_isLoop=isLoop;
			_count=count;
			_isAtToZero=isAtToZero;

			if (bitmapMovieClip == null)
				bitmapMovieClip=new BitmapMovieClip();

			_url=url;
			_delay=delay;
			bitmapMovieClip.x=_x;
			bitmapMovieClip.y=_y;
			bitmapMovieClip.delay=delay;
			if (isScaleX)
			{
				bitmapMovieClip.scaleX=-1;
			}

			if (SourceCache.getInstance().has(_url))
			{
				if (_parent)
				{
					if (_isAtToZero)
						_parent.addChildAt(bitmapMovieClip, 0);
					else
						_parent.addChild(bitmapMovieClip);
					dispatchEvent(new Event(ADDED_EFFECT));
				}

				bitmapMovieClip.movieClipDataContainer=new MovieClipDataContainer(SourceCache.getInstance().getObject(_url) as MovieClipData);
				bitmapMovieClip.gotoAndPlay(1, _isLoop, null, null, playOver);
			}
			else
			{
				SourceCache.getInstance().addEventListener(Event.COMPLETE, onResLoaderCompleteHandler);
				SourceCache.getInstance().addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
				if (SourceCache.getInstance().isLoading(_url) == false)
				{
					SourceCache.getInstance().load(_url, true);
				}

			}
		}

		public function resetXAndY(x:int, y:int):void
		{
			bitmapMovieClip.x=x;
			bitmapMovieClip.y=y;
		}

		/**
		 * 设置动画缩放
		 * @param scaleX
		 * @param scaleY
		 */
		public function setScale(scaleX:Number, scaleY:Number):void
		{
			if (bitmapMovieClip)
			{
				bitmapMovieClip.scaleX=scaleX;
				bitmapMovieClip.scaleY=scaleY;
			}
		}

		/**
		 * 设置是否可见
		 * @param value
		 */
		public function set visible(value:Boolean):void
		{
			_visible=value;
			if (bitmapMovieClip)
				bitmapMovieClip.visible=value;
		}

		private function onResLoaderCompleteHandler(evt:CacheEvent):void
		{
			if (evt.url == _url)
			{
				bitmapMovieClip.movieClipDataContainer=new MovieClipDataContainer(SourceCache.getInstance().getObject(_url) as MovieClipData);
				bitmapMovieClip.gotoAndPlay(1, _isLoop, null, null, playOver);

				if (_parent)
				{
					if (_isAtToZero)
						_parent.addChildAt(bitmapMovieClip, 0);
					else
						_parent.addChild(bitmapMovieClip);
					dispatchEvent(new Event(ADDED_EFFECT));
				}

				bitmapMovieClip.visible=_visible;
			}

		}

		private function ioErrorHandler(e:CacheEvent):void
		{
			if (e.url == _url)
			{
				dispose();
			}
		}

		private function playOver():void
		{
			_count--;

			//剩余播放次数
			if (_count > 0)
			{
				if (bitmapMovieClip)
					bitmapMovieClip.gotoAndPlay(1, _isLoop, null, null, playOver);
				return;
			}

			dispose();
		}

		public function dispose():void
		{
			SourceCache.getInstance().removeEventListener(Event.COMPLETE, onResLoaderCompleteHandler);
			SourceCache.getInstance().removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			if (bitmapMovieClip)
			{
				if (bitmapMovieClip.parent)
					bitmapMovieClip.parent.removeChild(bitmapMovieClip);
				bitmapMovieClip.dispose();
			}
			dispatchEvent(new Event(DISPOSE_EFFECT));
		}
	}
}

