package commons.cache
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;

	import commons.LocalCacheManager;
	import commons.MovieClipData;

	import events.loader.CacheEvent;
	import events.loader.LocalCacheEvent;

	public class SourceCache extends EventDispatcher implements IRender
	{
		public var _cacheDic:Dictionary;
		public var _gcDic:Dictionary;
		private var _len:int;
		private var _loaderDic:Dictionary;
		private var _loadingDic:Dictionary;
		private var _renderList:Array;
		private var _memoryPoolList:Array;
		private var _movieClipDic:Dictionary;
		private var _bitmapFrameDic:Dictionary;
		private var _errorDic:Dictionary=new Dictionary();


		private var offsetX:Number=100;
		private var offsetY:Number=100;

		public static var frameRate:int;

		public function SourceCache()
		{
			_cacheDic=new Dictionary();
			_gcDic=new Dictionary();
			_loaderDic=new Dictionary();
			_loadingDic=new Dictionary();
			_len=0;
			_renderList=new Array();

			_movieClipDic=new Dictionary();
			_bitmapFrameDic=new Dictionary();
			_errorDic=new Dictionary();


			LocalCacheManager.getInstance().addEventListener(Event.COMPLETE, onCompleteHandler);
			LocalCacheManager.getInstance().addEventListener(IOErrorEvent.IO_ERROR, onOoErrorHandler);
			//TimerManager.getInstance().add(60000 * 3, tempGC);
		}
		private static var _instance:SourceCache;

		public static function getInstance():SourceCache
		{
			if (_instance == null)
			{
				_instance=new SourceCache();

			}
			return _instance;
		}

		public function put(str:String, obj:Object, overWrite:Boolean=false, canGC:Boolean=false):void
		{
			_gcDic[str]=canGC;
			if (_cacheDic[str] == undefined)
			{
				_len++;
				_cacheDic[str]=obj;
			}
			else
			{
				if (overWrite)
				{
					if (_cacheDic[str] is BitmapData)
					{
						BitmapData(_cacheDic[str]).dispose();
					}
					if (_cacheDic[str] is MovieClipData)
					{
						MovieClipData(_cacheDic[str]).dispose();
					}
					_cacheDic[str]=obj;
				}
			}

			if (_cacheDic[str] is MovieClipData)
			{
				MovieClipData(_cacheDic[str]).url=str;
				MovieClipData(_cacheDic[str]).canGC=canGC;
			}
		}

		public function remove(str:String):void
		{
			if (_cacheDic[str] is BitmapData)
			{
				BitmapData(_cacheDic[str]).dispose();
			}
			else if (_cacheDic[str] is MovieClipData)
			{
				MovieClipData(_cacheDic[str]).dispose();
			}
			delete _cacheDic[str];
			delete _gcDic[str];
			delete _loadingDic[str];
			if (_movieClipDic[str] is MovieClip)
				MovieClip(_movieClipDic[str]).stop();
			delete _movieClipDic[str];
			_len--;
		}



		public function getObject(str:String):Object
		{
			return _cacheDic[str];
		}

		public function has(str:String):Boolean
		{
			return _cacheDic[str] != undefined
		}

		public function get count():int
		{
			return _len;
		}

		/**
		 *
		 * @param str
		 * @param canGC
		 *
		 */
		public function load(str:String, canGC:Boolean=false, loadFirst:Boolean=false):void
		{
			if (_errorDic[str] > 3)
			{
				_loadingDic[str]=true;
				return;
			}
			_gcDic[str]=canGC;
			_loadingDic[str]=true;
			LocalCacheManager.getInstance().loadFile(str, loadFirst);
		}

		private function onCompleteHandler(evt:LocalCacheEvent):void
		{
			var url:String=evt.url;
			if (_loadingDic[url] != true)
				return;
			if (url.substr(-3, 3) == "swf")
			{
				changeSwfToMovieClipDate(url, evt.data as MovieClip)
			}
			else if (url.substr(-2, 2) == "ng" || url.substr(-2, 2) == "pg")
			{

				var myBitmap:BitmapData=Bitmap(evt.data).bitmapData;
				SourceCache.getInstance().put(url, myBitmap, false, _gcDic[url]);
				SourceCache.getInstance().dispatchEvent(new CacheEvent(Event.COMPLETE, url));
			}

			delete _loadingDic[url];
		}

		/**
		 *
		 * @param mc
		 * 转换SWF 为movieClipData
		 */
		public function changeSwfToMovieClipDate(url:String, movie:MovieClip):void
		{
			movie.stop();
			_movieClipDic[url]=movie;

			var bitmapData:MovieClipData=new MovieClipData();
			var labelDic:Dictionary=new Dictionary();
			var arr:Array=new Array();
			for (var i:int=0; i < movie.totalFrames; i++)
			{
				movie.gotoAndStop(i);
				arr.push(null);
				if (movie.currentLabel != null && labelDic[movie.currentLabel] == undefined)
				{
					labelDic[movie.currentLabel]=movie.currentFrame;
				}
			}
			bitmapData.bitmapFrameList=arr;
			bitmapData.labelDic=labelDic;
			bitmapData.url=url;
			bitmapData.canGC=_gcDic[url];
			bitmapData.mc=movie;
			movie.gotoAndStop(1);
			SourceCache.getInstance().put(url, bitmapData, false, _gcDic[url]);
			SourceCache.getInstance().dispatchEvent(new CacheEvent(Event.COMPLETE, url));
		}

		private function onOoErrorHandler(evt:LocalCacheEvent):void
		{
			var url:String=evt.url;
			if (_loadingDic[url] != true)
				return;
			delete _loadingDic[url];
			SourceCache.getInstance().dispatchEvent(new CacheEvent(IOErrorEvent.IO_ERROR, url));
			if (_errorDic[url] == undefined)
				_errorDic[url]=1;
			else
				_errorDic[url]+=1;
			trace("资源加载失败：" + url);
		}

		public function isLoading(str:String):Boolean
		{
			return _loadingDic[str] == true
		}

		public function gc():void
		{
			for (var i:* in _cacheDic)
			{
				if (_gcDic[i] == true)
				{
					//LocalCacheManager.getInstance().stopLoding(_gcDic[i]);
					remove(i);
				}
			}

			for (var m:* in _loadingDic) ////////清空加载队列里面的数据
			{
				if (_gcDic[m] == true && _loadingDic[m] == true)
				{
					//LocalCacheManager.getInstance().stopLoding(m);
					delete _gcDic[m];
					delete _loadingDic[m];
				}
			}
			MovieClipData.dispose(); ////所有可以GC的位图数据统一再清理一次
		}

		/**
		 *定时回收位图数据
		 *
		 */
		public function tempGC():void
		{
			trace("******tempGC*******:::" + getTimer());
		}

		private var temp:Number=0;

		public function step():void
		{

		}
	}
}


