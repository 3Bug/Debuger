package core
{
	import flash.display.Stage;

	/**
	 *舞台数据
	 * @author Dont小鬼
	 *2014-3-7
	 */
	public class StageData
	{
		private static var _stage:Stage;

		public static var stageWidth:uint=0;

		public static var stageHeight:uint=0;

		public static function get stage():Stage
		{
			return _stage;
		}

		public static function set stage(value:Stage):void
		{
			_stage=value;
		}

		/**比率*/
		public static function ratio():Number
		{
			return (StageData.stageWidth / 400);
		}
	}
}


