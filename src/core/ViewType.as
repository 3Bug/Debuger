package core
{
	import View.ViewBase;
	import View.game.GameView;
	import View.help.HelpView;
	import View.welcome.WelcomeView;

	/**
	 * 界面类型
	 * @author Dont小鬼
	 */
	public class ViewType
	{
		/**欢迎界面**/
		public static const Welcome:String="ViewType.Welcome";

		/**游戏界面**/
		public static const Game:String="ViewType.Game";

		/**帮助界面**/
		public static const Help:String="ViewType.Help";

		/**关于我们界面**/
		public static const AbutUs:String="ViewType.AbutUs";

		//////////////////////////////////////////////////
		/****欢迎界面**/
		private static var _welcomePanel:WelcomeView;
		/**帮助界面**/
		private static var _helpPanel:HelpView;
		private static var _gamePanel:GameView;

		public static function getView(type:String):ViewBase
		{
			var view:ViewBase;
			switch (type)
			{
				case ViewType.Welcome:
					if (_welcomePanel == null)
						_welcomePanel=new WelcomeView();
					view=_welcomePanel;
					break;
				case ViewType.Game:
					if (_gamePanel == null)
						_gamePanel=new GameView();
					view=_gamePanel;
					break;
				case ViewType.Help:
					if (_helpPanel == null)
						_helpPanel=new HelpView();
					view=_helpPanel;
					break;
				default:
					if (_welcomePanel == null)
						_welcomePanel=new WelcomeView();
					view=_welcomePanel;
					break;
			}
			return view;
		}
	}
}
