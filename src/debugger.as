package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageAspectRatio;
	import flash.display.StageDisplayState;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	import flash.events.StageOrientationEvent;

	import View.MainCtrl;


	[SWF(frameRate="30", backgroundColor="0x000000")]
	/**
	 *@author Dont小鬼
	 *   _(:з」∠)_ (神兽坐镇，代码无BUG)
	 */
	public class debugger extends Sprite
	{

		public function debugger()
		{
			super();
			stage.align=StageAlign.TOP_LEFT;
			stage.scaleMode=StageScaleMode.SHOW_ALL;
			stage.displayState=StageDisplayState.FULL_SCREEN_INTERACTIVE
			retSetStageDirection(null);
			stage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGE, retSetStageDirection);
			stage.setAspectRatio(StageAspectRatio.PORTRAIT);
			this.addChild(MainCtrl.getInstance());
		}

		/**
		 * 调整舞台
		 * @param event
		 */
		private function retSetStageDirection(event:StageOrientationEvent):void
		{
			//屏幕保持默认方向      不设定自动重定向
			var startOrientation:String=stage.orientation;
			if (startOrientation == StageOrientation.ROTATED_LEFT || startOrientation == StageOrientation.
				ROTATED_RIGHT)
			{
				stage.setOrientation(StageOrientation.DEFAULT);
			}
			else
			{
				//调整舞台
				stage.setOrientation(startOrientation);
			}
		}
	}
}


