package events
{
	import flash.events.Event;

	/**
	 * 打开界面事件
	 * @author Dont小鬼
	 */
	public class ViewOpenEvent extends Event
	{
		/**打开界面**/
		public static const OpenView:String="ViewOpenEvent.OpenView";

		/**关闭界面**/
		public static const CloseView:String="ViewOpenEvent.CloseView";

		private var _data:*;
		private var _viewType:String;

		/**
		 * 打开界面事件
		 * @param type         打开类型     关闭/打开
		 * @param viewType     界面类型
		 * @param data         附带数据
		 */
		public function ViewOpenEvent(type:String, viewType, data:*=null)
		{
			_viewType=viewType;
			_data=data;
			super(type);
		}

		public function get viewType():String
		{
			return _viewType;
		}

		public function get data():*
		{
			return _data;
		}

	}
}
