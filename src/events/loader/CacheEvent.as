package events.loader
{
	import flash.events.Event;

	public class CacheEvent extends Event
	{
		public var url:String;

		public function CacheEvent(type:String, url:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.url=url;
			super(type, bubbles, cancelable);
		}

	}
}
