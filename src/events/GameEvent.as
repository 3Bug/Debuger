package events
{
	import flash.events.Event;

	/**
	 *游戏事件
	 *@author Dont小鬼
	 *
	 *   _(:з」∠)_ (神兽坐镇，代码无BUG)
	 *2014-10-3
	 */
	public class GameEvent extends Event
	{
		/**游戏开始**/
		public static const GameStart:String="GameEvent.GameStart";

		/**游戏结束**/
		public static const GameOver:String="GameEvent.GameOver";

		/**游戏暂停**/
		public static const GamePause:String="GameEvent.GamePause";

		/**使用技能**/
		public static const UseSkill:String="GameEvent.UseSkill";

		private var _data:*;

		public function GameEvent(type:String, data:*=null)
		{
			_data=data;
			super(type);
		}

		public function get data():*
		{
			return _data;
		}

	}
}


