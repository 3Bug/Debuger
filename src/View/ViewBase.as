package View
{
	import flash.display.Sprite;

	public class ViewBase extends Sprite
	{
		public function ViewBase()
		{
			super();
		}

		public function show():void
		{

		}

		public function hide():void
		{

		}
	}
}
