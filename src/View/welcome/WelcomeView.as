package View.welcome
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	import View.ViewBase;

	import commons.ImgBitmap;

	import core.ViewType;

	import events.ViewOpenEvent;

	import webgame.core.bus.GameBus;

	public class WelcomeView extends ViewBase
	{
		private var _startBtn:Sprite;
		private var _helpBtn:Sprite;
		private var _aboutBtn:Sprite;

		public function WelcomeView()
		{
			super();
			initView();
		}

		override public function show():void
		{
			addListener();
		}

		override public function hide():void
		{
			removeListener();
		}

		/**
		 * 移除事件侦听
		 */
		private function removeListener():void
		{
			_startBtn.removeEventListener(MouseEvent.CLICK, onStartClick);
		}

		/**
		 * 添加事件侦听
		 */
		private function addListener():void
		{
			_startBtn.addEventListener(MouseEvent.CLICK, onStartClick);
		}

		/**
		 * 初始化界面
		 */
		private function initView():void
		{
			var bit:ImgBitmap=new ImgBitmap("welcome/bug.png");
			bit.scaleX=2.5;
			bit.scaleY=2.5;
			this.addChild(bit);

			_startBtn=new Sprite();
			_startBtn.buttonMode=true;
			bit=new ImgBitmap("welcome/start.png");
			_startBtn.addChild(bit);
			_startBtn.x=230;
			_startBtn.y=330;
			this.addChild(_startBtn);
		}

		/**
		 * 点击开始按钮
		 * @param event
		 */
		private function onStartClick(event:MouseEvent):void
		{
			GameBus.getInstance().dispatchEvent(new ViewOpenEvent(ViewOpenEvent.OpenView, ViewType.Game));
		}

	}
}


