package View
{

	import core.ViewType;

	public class MainView extends ViewBase
	{
		/**当前界面*/
		private var _curView:ViewBase=null;


		public function MainView()
		{
			super();
			initView();
		}

		override public function show():void
		{
			if (_curView)
				_curView.show();
		}

		override public function hide():void
		{
			if (_curView)
				_curView.hide();
		}

		/**
		 * 初始化界面
		 */
		private function initView():void
		{
			showView(ViewType.Welcome);
		}

		/**
		 * 显示界面
		 */
		public function showView(type:String):void
		{
			if (_curView)
			{
				_curView.hide();
				if (this.contains(_curView))
					this.removeChild(_curView);
			}

			_curView=ViewType.getView(type);

			if (_curView)
			{
				this.addChild(_curView);
				_curView.show();
			}
		}

		/**
		 * 隐藏界面
		 */
		public function hideView():void
		{
			if (_curView)
			{
				_curView.hide();
				_curView=null;
			}
		}
	}
}


