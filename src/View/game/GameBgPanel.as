package View.game
{
	import View.ViewBase;

	/**
	 * 游戏背景容器
	 * @author Dont小鬼
	 * 2014-9-28
	 */
	public class GameBgPanel extends ViewBase
	{
		public function GameBgPanel()
		{
			super();
			initView();
		}

		override public function show():void
		{

		}

		override public function hide():void
		{

		}

		/**
		 * 初始化界面
		 */
		private function initView():void
		{

		}
	}
}

