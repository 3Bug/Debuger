package View.game
{
	import View.ViewBase;

	import events.GameEvent;

	import webgame.core.bus.GameBus;
	import webgame.utils.timer.TimerManager;

	/**
	 * 主游戏界面
	 * @author Dont小鬼
	 * 2014-9-28
	 */
	public class GameView extends ViewBase
	{
		/**菜单容器*/
		private var _menuPanel:GameMenuPanel;
		private var _bgPanel:GameBgPanel;
		private var _contentPanel:GameContentPanel;

		public function GameView()
		{
			super();
			initView();
		}

		override public function show():void
		{
			_bgPanel.show();
			_menuPanel.show();
			_contentPanel.show();
			start();
		}

		override public function hide():void
		{
			stop();
			_contentPanel.hide();
			_bgPanel.hide();
			_menuPanel.hide();
		}

		/**
		 * 添加事件侦听
		 */
		private function addListener():void
		{
			GameBus.getInstance().addEventListener(GameEvent.GameOver, gameOver);
		}

		/**
		 * 移除事件侦听
		 */
		private function removeListener():void
		{
			GameBus.getInstance().removeEventListener(GameEvent.GameOver, gameOver);
		}

		/**
		 * 游戏开始
		 */
		private function start():void
		{
			TimerManager.getInstance().add(30, onFrame);
			addListener();
		}

		/**
		 * 游戏停止
		 */
		private function stop():void
		{
			TimerManager.getInstance().remove(onFrame);
			_contentPanel.stopGame();

			removeListener();
		}

		/**
		 * 游戏结束
		 */
		private function gameOver(event:GameEvent=null):void
		{
			TimerManager.getInstance().remove(onFrame);
			_contentPanel.pause(2);

			removeListener();
		}

		/**
		 * 游戏暂停
		 */
		private function pause():void
		{
			TimerManager.getInstance().remove(onFrame);
			_contentPanel.pause(1);

			removeListener();
		}

		/**
		 * 帧计时器
		 */
		private function onFrame():void
		{
			_contentPanel.onFrame();
		}

		/**
		 * 初始化界面
		 */
		private function initView():void
		{
			_bgPanel=new GameBgPanel();
			this.addChild(_bgPanel);

			_contentPanel=new GameContentPanel();
			this.addChild(_contentPanel);

			_menuPanel=new GameMenuPanel();
			this.addChild(_menuPanel);
		}

		/**
		 * 获取内容容器
		 * @return
		 */
		public function getContentPanel():GameContentPanel
		{
			return _contentPanel;
		}
	}
}

