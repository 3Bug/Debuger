package View.game.skill
{
	import core.StageData;

	/**
	 * 技能类型
	 *@author Dont小鬼
	 *
	 *   _(:з」∠)_ (神兽坐镇，代码无BUG)
	 *2014-10-9
	 */
	public class SkillsType
	{
		/**绿色技能**/
		public static const SkillGreen:uint=1;

		/**蓝色技能**/
		public static const SkillBlue:uint=2;

		/**紫色技能**/
		public static const SkillPurple:uint=3;

		/**黄色技能**/
		public static const SkillYellow:uint=4;

		public function SkillsType()
		{
		}

		/**
		 * 获取对于技能的速度
		 * @return
		 */
		public static function getSkillSpeed(type:uint):uint
		{
			var sp:uint=0;
			switch (type)
			{
				case SkillGreen:
					sp=8;
					break;
				case SkillBlue:
					sp=0;
					break;
				case SkillPurple:
					sp=4;
					break;
				case SkillYellow:
					sp=6;
					break;
			}
			return sp * StageData.ratio();
		}
	}
}


