package View.game.skill
{
	import flash.display.Sprite;
	import flash.errors.IllegalOperationError;
	import flash.geom.Point;
	import flash.utils.Dictionary;

	import View.game.GameContentPanel;
	import View.game.GameView;

	import core.ViewType;

	import events.GameEvent;

	import webgame.core.bus.GameBus;

	/**
	 * 技能控制器
	 *@author Dont小鬼
	 *
	 *   _(:з」∠)_ (神兽坐镇，代码无BUG)
	 *2014-10-9
	 */
	public class SkillCtrl
	{
		private static var _instance:SkillCtrl=null;
		private static var _allowInstance:Boolean=false;

		/***技能列表**/
		private var _skillList:Dictionary;
		/**技能容器*/
		private var _skillSp:Sprite;

		private var _times:int;

		public function SkillCtrl()
		{
			if (!_allowInstance)
				throw new IllegalOperationError("不能直接实例化SkillCtrl类");

			_skillList=new Dictionary();
			_skillSp=getSkillSp();
			GameBus.getInstance().addEventListener(GameEvent.UseSkill, addSkill);
		}

		public static function getInstance():SkillCtrl
		{
			if (null == _instance)
			{
				_allowInstance=true;
				_instance=new SkillCtrl();
				_allowInstance=false;
			}
			return _instance;
		}

		public function onFrame():void
		{
			_times++;
			moveSkill();
		}

		/**
		 * 移动技能
		 */
		private function moveSkill():void
		{
			var skill:SkillSpr;
			for each (skill in _skillList)
			{
				skill.y-=SkillsType.getSkillSpeed(skill.type);
				if (checkSkillAlive(skill, _times) == false)
					removeSkill(uint(skill.name));
			}
		}

		/**
		 * 检查技能是否存活
		 * @return
		 */
		private function checkSkillAlive(skill:SkillSpr, times:uint):Boolean
		{
			var alive:Boolean=false;
			switch (skill.type)
			{
				case SkillsType.SkillGreen:
				case SkillsType.SkillPurple:
				case SkillsType.SkillYellow:
					if (skill.y < 0 - skill.height + 80)
						alive=false
					else
						alive=true;
					break;
				case SkillsType.SkillBlue:
					if (times - uint(skill.name) > 60)
						alive=false
					else
						alive=true;
					break;
			}
			return alive;
		}

		/**
		 * 添加技能
		 * @param event
		 */
		public function addSkill(skillId:uint, pos:Point):void
		{
			if (pos && skillId != 0)
				newSkill(_times, skillId, pos);
		}

		/**
		 * 移除技能
		 * @param key
		 */
		public function removeSkill(key:uint):void
		{
			if (_skillList.propertyIsEnumerable(key) == false)
				return;

			var skill:SkillSpr=_skillList[key] as SkillSpr;
			if (skill)
			{
				if (_skillSp.contains(skill))
					_skillSp.removeChild(skill);
			}
			delete _skillList[key];
		}

		/**
		 * 实例化并添加技能容器
		 */
		private function newSkill(times:uint, skillId:uint, pos:Point):void
		{
			var skill:SkillSpr=new SkillSpr(skillId);
			skill.x=pos.x;
			skill.y=pos.y;
			skill.name=times.toString();
			skill.startEff();
			_skillSp.addChild(skill);
			_skillList[times]=skill;
		}

		/**
		 * 获取技能容器
		 * @return
		 */
		private function getSkillSp():Sprite
		{
			var view:GameView=ViewType.getView(ViewType.Game) as GameView;
			var contentSp:GameContentPanel=null;
			var skillSp:Sprite=null;
			if (view)
				contentSp=view.getContentPanel() as GameContentPanel;

			if (contentSp)
				skillSp=contentSp.contentPanel;
			return skillSp;
		}

		/**技能列表**/
		public function get skillList():Dictionary
		{
			return _skillList;
		}

	}
}
