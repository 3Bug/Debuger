package View.game.skill
{
	import flash.display.Sprite;

	import mx.utils.StringUtil;

	import commons.Effect;

	import core.StageData;

	/**
	 * 单个技能容器
	 *@author Dont小鬼
	 *
	 *   _(:з」∠)_ (神兽坐镇，代码无BUG)
	 *2014-10-9
	 */
	public class SkillSpr extends Sprite
	{
		private const Path:String="game/skill{0}.swf";
		private var _type:uint;
		private var _bg:Sprite;
		private var _skill:Effect;

		public function SkillSpr(type:uint)
		{
			_type=type;
			super();
			initView();
		}

		/**
		 * 播放特效
		 */
		public function startEff():void
		{
			switch (_type)
			{
				case SkillsType:

					break;
			}
			_skill.showEffect(StringUtil.substitute(Path, _type), this, 0, 0, 80, true);
			_skill.setScale(StageData.ratio(), StageData.ratio());
		}

		/**
		 * 销毁特效
		 */
		public function disposeEff():void
		{
			_skill.dispose();
		}

		/**
		 * 初始化界面
		 */
		private function initView():void
		{
			_skill=new Effect();
		}

		public function get type():uint
		{
			return _type;
		}
	}
}


