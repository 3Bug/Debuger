package View.game
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;

	import View.ViewBase;
	import View.game.gestrues.GameDrawPanel;
	import View.game.monster.MonsterCtrl;
	import View.game.monster.NomalMonsterSpr;
	import View.game.skill.SkillCtrl;
	import View.game.skill.SkillSpr;

	import commons.CustomTextfield;
	import commons.StringUtils;

	import core.StageData;

	import gestrues.DollarRecognizer;
	import gestrues.DollarResult;
	import gestrues.PreTemplate;

	import webgame.utils.timer.TimerManager;

	/**
	 * 游戏主体容器
	 * @author Dont小鬼
	 * 2014-9-28
	 */
	public class GameContentPanel extends ViewBase
	{
		/**内容容器*/
		private var _contentPanel:Sprite;
		private var _drawPanel:GameDrawPanel;
		private var _recognizer:DollarRecognizer;

		public function GameContentPanel()
		{
			super();
			initView();
		}

		override public function show():void
		{
			startGame();
		}

		override public function hide():void
		{
			stopGame();
		}

		/**
		 * 帧计时器
		 */
		public function onFrame():void
		{
			MonsterCtrl.getInstance().onFrame();
			SkillCtrl.getInstance().onFrame();
			_drawPanel.onFarme();
		}

		/**
		 * 碰撞检测
		 */
		private function hitTest():void
		{
			var skillDic:Dictionary=SkillCtrl.getInstance().skillList;
			var monsterDic:Dictionary=MonsterCtrl.getInstance().monsterList;
			if (skillDic == null || monsterDic == null)
				return;
			var skill:SkillSpr=null;
			var monster:NomalMonsterSpr=null;
			for each (skill in skillDic)
			{
				for each (monster in monsterDic)
				{
					if (skill.type == monster.type && skill.hitTestObject(monster))
						MonsterCtrl.getInstance().removeMonster(monster);
				}
			}
		}

		/**
		 * 停止游戏
		 */
		public function stopGame():void
		{
			pause();
			_drawPanel.cleanGraphics();
			while (_contentPanel.numChildren > 0)
				_contentPanel.removeChildAt(0);
		}

		/**
		 * 开始游戏
		 */
		public function startGame():void
		{
			_drawPanel.addListener();
			_drawPanel.addEventListener(MouseEvent.MOUSE_UP, onDrawCom);
			TimerManager.getInstance().add(300, hitTest);
		}

		/**
		 * 暂停游戏
		 * @param viewType  打开界面类型   0为不打开
		 */
		public function pause(viewType:uint=0):void
		{
			_drawPanel.removeListener();
			_drawPanel.removeEventListener(MouseEvent.MOUSE_UP, onDrawCom);
			TimerManager.getInstance().remove(hitTest);
			switch (viewType)
			{
				case 1: //打开暂停界面
					break;
				case 2: //打开游戏结束界面
					break;
			}
		}

		/**
		 * 初始化界面
		 */
		private function initView():void
		{
			_contentPanel=new Sprite();
			this.addChild(_contentPanel);

			_recognizer=new DollarRecognizer(PreTemplate.getTempLateList());

			_drawPanel=new GameDrawPanel();
			this.addChild(_drawPanel);

			var tes:CustomTextfield=new CustomTextfield();
			tes.htmlText=StringUtils.substitute("wid:{0},hei:{1}", StageData.stageWidth, StageData.stageHeight);
			this.addChild(tes);
		}

		/**
		 * 绘画完毕
		 * @param event
		 */
		private function onDrawCom(event:MouseEvent):void
		{
			var res:DollarResult=_recognizer.recognize(_drawPanel.getPoints());
			if (res && res.template)
			{ //使用技能
				SkillCtrl.getInstance().addSkill(res.template.id, res.firstPoint);
			}
		}

		public function get contentPanel():Sprite
		{
			return _contentPanel;
		}
	}
}

