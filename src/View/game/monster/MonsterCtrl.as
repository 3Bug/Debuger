package View.game.monster
{
	import flash.display.Sprite;
	import flash.errors.IllegalOperationError;
	import flash.utils.Dictionary;

	import View.game.GameContentPanel;
	import View.game.GameView;

	import core.StageData;
	import core.ViewType;

	/**
	 * 怪物控制器
	 *@author Dont小鬼
	 *
	 *   _(:з」∠)_ (神兽坐镇，代码无BUG)
	 *2014-10-3
	 */
	public class MonsterCtrl
	{
		private static var _instance:MonsterCtrl=null;
		private static var _allowInstance:Boolean=false;
		private var _times:int;
		private var _monSp:Sprite=null;
		private var _addSpeed:uint=30;
		/**怪物列表**/
		private var _monsterList:Dictionary;

		public function MonsterCtrl()
		{
			if (!_allowInstance)
			{
				throw new IllegalOperationError("不能直接实例化MonsterCtrl类");
			}
			_monsterList=new Dictionary();
			_monSp=getMonsterSp();
		}

		public static function getInstance():MonsterCtrl
		{
			if (null == _instance)
			{
				_allowInstance=true;
				_instance=new MonsterCtrl();
				_allowInstance=false;
			}
			return _instance;
		}

		/**
		 * 帧计时器
		 */
		public function onFrame():void
		{
			_times++;
			addMonster(_times);
			moveMonster();
		}

		/**
		 * 设置怪物生成速度
		 */
		private function setAddSpeed(times:uint):void
		{
			//第一分钟内
			if (times < 20000)
				_addSpeed=40;
			else
				_addSpeed=Math.round(Math.random() * 10) + 15;

			//测试速率
			_addSpeed=40;
		}

		/**
		 * 移动怪物
		 */
		private function moveMonster():void
		{
			var mon:NomalMonsterSpr=null;
			for each (mon in _monsterList)
			{
				mon.y+=mon.moveSpeed;
				if (mon.y > StageData.stageHeight)
				{ //游戏结束
					removeMonsterByKey(uint(mon.name));
						//GameBus.getInstance().dispatchEvent(new GameEvent(GameEvent.GameOver));
						//return;
				}
			}
		}

		/**
		 * 生成怪物
		 */
		public function addMonster(times:uint):void
		{
			if (_monSp == null)
				return;
			//设置怪物生成速度
			setAddSpeed(times);

			//生成怪物
			if (_times % _addSpeed == 0)
				newMonster(_times);
		}

		/**
		 * 移除怪物
		 * @param key
		 */
		public function removeMonsterByKey(key:uint):void
		{
			if (_monsterList.propertyIsEnumerable(key) == false)
				return;

			var mon:NomalMonsterSpr=_monsterList[key] as NomalMonsterSpr;
			removeMonster(mon);
		}

		/**
		 * 移除怪物
		 * @param mon
		 */
		public function removeMonster(mon:NomalMonsterSpr):void
		{
			if (mon)
			{
				if (_monSp.contains(mon))
					_monSp.removeChild(mon);
			}
			delete _monsterList[uint(mon.name)];
		}

		/**
		 * 实例化并添加怪物
		 */
		private function newMonster(times:uint):void
		{
			var mon:NomalMonsterSpr=new NomalMonsterSpr(MonsterType.randomNormalMonster(), MonsterType.
				randomNorMosSpeed());
			mon.x=Math.random() * (StageData.stageWidth - mon.getWid() * 2);
			mon.y=0;
			mon.name=times.toString();
			mon.startEff();
			_monSp.addChild(mon);
			_monsterList[times]=mon;
		}

		/**
		 * 获取怪物容器
		 * @return
		 */
		private function getMonsterSp():Sprite
		{
			var view:GameView=ViewType.getView(ViewType.Game) as GameView;
			var contentSp:GameContentPanel=null;
			var monSp:Sprite=null;
			if (view)
				contentSp=view.getContentPanel() as GameContentPanel;

			if (contentSp)
				monSp=contentSp.contentPanel;
			return monSp;
		}

		/**怪物列表**/
		public function get monsterList():Dictionary
		{
			return _monsterList;
		}

	}
}


