package View.game.monster
{
	import core.StageData;

	/**
	 *怪物类型
	 *@author Dont小鬼
	 *
	 *   _(:з」∠)_ (神兽坐镇，代码无BUG)
	 *2014-10-3
	 */
	public class MonsterType
	{
		/**Boss1号**/
		public static var Boss1:uint=10001;

		/**怪物类型1**/
		public static var Nomal1:uint=1;
		/**怪物类型2**/
		public static var Nomal2:uint=2;
		/**怪物类型3**/
		public static var Nomal3:uint=3;
		/**怪物类型4**/
		public static var Nomal4:uint=4;


		/**
		 * 随机一个普通怪物类型
		 * @return
		 */
		public static function randomNormalMonster():uint
		{
			return Math.round(Math.random() * 3) + 1;
		}

		/**
		 * 随机一个普通怪物速度
		 * @return
		 */
		public static function randomNorMosSpeed():uint
		{
			return Math.round(Math.random() * 5 * StageData.ratio()) + 4;
		}
	}
}


