package View.game.monster
{
	import flash.display.Sprite;

	import mx.utils.StringUtil;

	import commons.Effect;

	import core.StageData;

	/**
	 *普通怪物容器
	 *@author Dont小鬼
	 *
	 *   _(:з」∠)_ (神兽坐镇，代码无BUG)
	 *2014-10-3
	 */
	public class NomalMonsterSpr extends Sprite
	{
		private const Path:String="game/monster_{0}.swf";

		private var _monster:Effect;
		/**怪物类型**/
		private var _type:uint=1;
		private var _bg:Sprite;
		private var _wid:uint;
		private var _hei:uint;
		/***移动速度**/
		private var _moveSpeed:uint;

		public function NomalMonsterSpr(type:uint, moveSpeed:uint, bgWid:uint=55, bgHei:uint=55)
		{
			_type=type;
			_moveSpeed=moveSpeed;
			_wid=bgWid * StageData.ratio();
			_hei=bgHei * StageData.ratio();
			super();
			initView();
		}

		/**
		 * 播放特效
		 */
		public function startEff():void
		{
			_monster.showEffect(StringUtil.substitute(Path, _type), this, 0, 0, 80, true);
			_monster.setScale(StageData.ratio(), StageData.ratio());
		}

		/**
		 * 销毁特效
		 */
		public function disposeEff():void
		{
			_monster.dispose();
		}

		/**
		 * 初始化界面
		 */
		private function initView():void
		{
			/*_bg=new Sprite();
			_bg.graphics.drawRect(0, 5, _wid, _hei);
			_bg.alpha=0;
			this.addChild(_bg);*/
			_monster=new Effect();
		}

		public function getWid():uint
		{
			return _wid;
		}

		public function get moveSpeed():uint
		{
			return _moveSpeed;
		}

		/**怪物类型**/
		public function get type():uint
		{
			return _type;
		}

	}
}


