package View.game
{
	import View.ViewBase;

	/**
	 * 游戏菜单容器
	 * @author Dont小鬼
	 * 2014-9-28
	 */
	public class GameMenuPanel extends ViewBase
	{
		public function GameMenuPanel()
		{
			super();
			initView();
		}

		override public function show():void
		{

		}

		override public function hide():void
		{

		}

		/**
		 * 初始化界面
		 */
		private function initView():void
		{

		}
	}
}

