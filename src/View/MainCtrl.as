package View
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.system.Capabilities;

	import core.StageData;

	import events.ViewOpenEvent;

	import webgame.core.bus.GameBus;

	/**
	 * 主控制类
	 * @author Dont小鬼
	 */
	public class MainCtrl extends Sprite
	{
		private static var _instance:MainCtrl=null;
		private var _mainView:MainView;

		public function MainCtrl()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddToStage);
		}

		public static function getInstance():MainCtrl
		{
			if (null == _instance)
				_instance=new MainCtrl();
			return _instance;
		}

		/**
		 * 添加到场景
		 * @param event
		 */
		private function onAddToStage(event:Event):void
		{
			StageData.stage=this.stage;
			//StageData.stageWidth=Capabilities.screenResolutionX;
			//StageData.stageHeight=Capabilities.screenResolutionY;
			StageData.stageWidth=482;
			StageData.stageHeight=790;

			_mainView=new MainView();
			_mainView.show();
			StageData.stage.addChild(_mainView);
			GameBus.getInstance().addEventListener(ViewOpenEvent.OpenView, openView);
			GameBus.getInstance().addEventListener(ViewOpenEvent.CloseView, closeView);
		}

		/**
		 * 打开界面
		 * @param event
		 */
		private function openView(event:ViewOpenEvent):void
		{
			_mainView.showView(event.viewType);
		}

		/**
		 * 关闭界面
		 * @param event
		 */
		private function closeView(event:ViewOpenEvent):void
		{
			_mainView.hide();
		}

	}
}
