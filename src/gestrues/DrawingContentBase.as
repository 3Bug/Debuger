package gestrues
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Point;

	import core.StageData;

	/**
	 * 手势绘图容器基础
	 * @author Dont小鬼
	 * 2014-10-8
	 */
	public class DrawingContentBase extends Sprite
	{
		protected var _points:Array;
		protected var _lineThickness:Number=1;
		protected var _lineColor:Number=0xffffff;
		protected var _lineAlpha:Number=1;
		protected var _drawPoints:Boolean;
		protected var _lineDirty:Boolean;
		private var _isMouseDown:Boolean=false;
		private var _times:uint=0;

		/**
		 *获取黄色滤镜值
		 */
		public static const yellowFilter:Array=[new GlowFilter(0x00ff24, 0.5
			, 6, 6, 3)];

		public function DrawingContentBase()
		{
			super();
			//this.filters=yellowFilter;
		}

		/**
		 * 添加事件侦听
		 */
		public function addListener():void
		{
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}

		/**
		 * 移除事件侦听
		 */
		public function removeListener():void
		{
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}

		protected function onMouseDown(ev:MouseEvent):void
		{
			_points=new Array();
			var p:Point=new Point(ev.localX, ev.localY);
			_points.push(p);
			prepareDraw(p);
			drawPoint(p);
			_times=0;
			_isMouseDown=true;
			addEventListener(MouseEvent.MOUSE_UP, onMouseUp, false, 0, true);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp, false, 0, true);
		}

		protected function drawPoint(p:Point):void
		{
			if (_drawPoints)
			{
				graphics.beginFill(_lineColor, _lineAlpha);
				graphics.drawCircle(p.x, p.y, _lineThickness);
				graphics.endFill();
			}
			else
			{
				graphics.lineTo(p.x, p.y);
			}
		}

		protected function prepareDraw(p:Point):void
		{
			graphics.clear();
			if (!_drawPoints)
			{
				graphics.lineStyle(_lineThickness, _lineColor, _lineAlpha);
			}
			graphics.moveTo(p.x, p.y);
		}

		public function onFarme():void
		{
			_times++;
			if (_isMouseDown && _times % 2 == 0)
				onMouseMove();
		}

		/**
		 * 鼠标移动
		 */
		protected function onMouseMove():void
		{
			var p:Point=new Point(StageData.stage.mouseX, StageData.stage.mouseY);
			_points.push(p);
			drawPoint(p);
		}

		protected function onMouseUp(ev:MouseEvent=null):void
		{
			_isMouseDown=false;
			removeEventListener(MouseEvent.MOUSE_UP, onMouseUp, false);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp, false);
			if (ev.currentTarget == stage)
			{
				dispatchEvent(ev);
			}
		}
	}
}

