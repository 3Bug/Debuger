package gestrues
{
	import flash.geom.Point;

	/**
	 * 单个样本数据
	 * @author Dont小鬼
	 * 2014-10-8
	 */
	public class DollarTemplate
	{
		public var id:uint;
		public var points:Array;
		public var firstPoints:Point

		public function DollarTemplate(id:uint=0, points:Array=null)
		{
			this.id=id;
			if (points && points.length > 1)
			{
				var temp:Point=points[0] as Point;
				firstPoints=new Point(temp.x, temp.y);
				this.points=DollarRecognizer.resample(points.concat(), DollarRecognizer.NUM_POINTS);
				this.points=DollarRecognizer.rotateToZero(this.points);
				this.points=DollarRecognizer.scaleToSquare(this.points, DollarRecognizer.SQUARE_SIZE);
				this.points=DollarRecognizer.translateToOrigin(this.points);
			}
		}
	}
}

