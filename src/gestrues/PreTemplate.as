package gestrues
{
	import flash.geom.Point;

	/**
	 *
	 *@author Dont小鬼
	 *
	 *   _(:з」∠)_ (神兽坐镇，代码无BUG)
	 *2014-10-8
	 */
	public class PreTemplate
	{

		public function PreTemplate()
		{
		}

		/**
		 * 获取预设的数据
		 * @return
		 */
		public static function getTempLateList():Array
		{
			var arr:Array=[];

			var temp:DollarTemplate=new DollarTemplate(1
				, [new Point(50, 50), new Point(50, 100), new Point(100, 100)]);
			arr.push(temp);

			temp=new DollarTemplate(2
				, [new Point(75, 75), new Point(50, 100), new Point(100, 100), new Point(75, 75)]);
			arr.push(temp);

			temp=new DollarTemplate(3
				, [new Point(50, 50), new Point(100, 50), new Point(50, 100), new Point(100, 100)]);
			arr.push(temp);

			temp=new DollarTemplate(4
				, [new Point(50, 50), new Point(100, 50), new Point(100, 100)]);
			arr.push(temp);

			return arr;
		}
	}
}
