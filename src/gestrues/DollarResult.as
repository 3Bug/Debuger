package gestrues
{
	import flash.geom.Point;

	/**
	 * 单个手势返回数据
	 * @author Dont小鬼
	 * 2014-10-8
	 */
	public class DollarResult
	{
		public var template:DollarTemplate;
		public var score:Number;
		public var firstPoint:Point;

		public function DollarResult(template:DollarTemplate, score:Number, firstPoint:Point)
		{
			this.template=template;
			this.score=score;
			this.firstPoint=firstPoint;
		}
	}
}

